<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update Result</title>
</head>
<body>
<h2>Update Results</h2>
<p>Number of rows affected: <%= request.getAttribute("updateCount") %></p>
</body>
</html>

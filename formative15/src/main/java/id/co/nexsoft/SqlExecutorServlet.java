package id.co.nexsoft;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet(name = "sqlExecutor", urlPatterns = {"/sqlExecutor"})
public class SqlExecutorServlet extends HttpServlet {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/custom_table";
    private static final String USER = "root";
    private static final String PASS = "admin123";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sqlQuery = request.getParameter("sqlQuery");
        request.setAttribute("sqlQuery", sqlQuery);
        
        try {
            // Load database driver and establish connection
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
                Statement stmt = conn.createStatement()) {
                
                if (sqlQuery.trim().toLowerCase().startsWith("select")) {
                    ResultSet rs = stmt.executeQuery(sqlQuery);
                    request.setAttribute("resultSet", rs);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("queryResult.jsp");
                    dispatcher.forward(request, response);
                } else {
                    // For non-SELECT queries, use executeUpdate
                    int result = stmt.executeUpdate(sqlQuery);
                    request.setAttribute("updateCount", result);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("updateResult.jsp");
                    dispatcher.forward(request, response);
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new ServletException("Database connection error", e);
        }
    }
}
